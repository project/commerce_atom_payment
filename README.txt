INTRODUCTION
-------------
This project integrates Atom into the http://drupal.org/project/commerce
payment and checkout systems.

REQUIREMENTS
------------
- Drupal Commerce and its dependencies.
- Commerce Payment.

INSTALLATION
------------
Install this module and all its required dependencies.
Once it's installed, the new payment structure will be generated.

CONFIGURATION
-------------
1) Goto /admin/commerce/config/payment-gateways/add.
2) Select Atom Redirect Payment option in plugin field.
3) Fill following necessary fields.
   - Mode.
   - Atom Login ID.
   - Atom Pass Code.
   - Atom Request key.
   - Atom Response key.
   - Atom Client Code.
4) Click Save.

HELP
----
This module has been developed with help of 
https://www.atomtech.in/webkits/Atom_Paynetz_PHP_Kit.zip PHP source code.
